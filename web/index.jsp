<%--
  Created by IntelliJ IDEA.
  User: student05
  Date: 7/9/20
  Time: 3:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Home</title>
    <%@include file="WEB-INF/partials/bootstrap.jsp"%>
  </head>
  <body>
  <%@include file="WEB-INF/partials/navbar.jsp"%>
  <div class="container">
  <h1>Welcome to my Servlet Project Home Page</h1>
  <div class="container">
    <a href="/login"><button>Login Page</button></a>
    <a href="items"><button>Wish-List Page</button></a>
  </div>
</div>
  <%@include file="WEB-INF/partials/footer.jsp"%>
  </body>
</html>
