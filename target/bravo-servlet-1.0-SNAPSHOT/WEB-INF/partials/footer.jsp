<%--
  Created by IntelliJ IDEA.
  User: student05
  Date: 7/13/20
  Time: 9:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Footer Partial</title>
    <style>
        footer {
            text-align: center;
            padding: 3px;
            background-color: DarkSalmon;
            color: white;
        }
    </style>
</head>
<body>
<footer>
    <p>
        Author: Jonathan Robles<br>
        <a href="mailto:jonathan.robles287@gmail.com">jonathan.robles287@gmail.com</a>
    </p>
</footer>
</body>
</html>
