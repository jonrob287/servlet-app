<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student05
  Date: 7/13/20
  Time: 8:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Page</title>
    <%@include file="WEB-INF/partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp"%>
<div class="container">
    <form method="post" action="/login" >
        <input type="text" placeholder="Username" name="username">
        <br>
        <input type="password" placeholder="Password" name="password">
        <br>
        <button type="submit">Login</button>
    </form>
</div>


<%@include file="WEB-INF/partials/footer.jsp"%>

</body>
