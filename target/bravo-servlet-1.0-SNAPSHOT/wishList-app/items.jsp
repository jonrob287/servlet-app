<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student05
  Date: 7/14/20
  Time: 8:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View WishList</title>
</head>
<body>
<main>
    <h3>Here is my CodeBound WishList:</h3>
    <table border="1">
        <tr>
            <th>Name</th>
            <th>Category</th>
            <th>Price</th>
        </tr>
        <c:forEach items="${listOfItems}" var="addedItems">
            <tr>
                <td>${addedItems.name}</td>
                <td>${addedItems.category}</td>
                <td>$${addedItems.price}</td>
            </tr>
        </c:forEach>
    </table>

<%--    <p>test</p>--%>

<%--    <c:forEach items="${listOfItems}" var="addedItems">--%>
<%--        <h3>${addedItems.name}</h3>--%>
<%--        <p>${addedItems.category}</p>--%>
<%--        <p>$${addedItems.price}</p>--%>
<%--    </c:forEach>--%>
    <a href="/items/add-item"><button>Add an Item</button></a>
</main>
</body>
</html>
