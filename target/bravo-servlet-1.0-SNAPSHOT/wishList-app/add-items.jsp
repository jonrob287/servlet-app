<%--
  Created by IntelliJ IDEA.
  User: student05
  Date: 7/14/20
  Time: 8:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Item</title>
</head>
<body>
<main>
    <h3>Enter the new item's info:</h3>
    <form action="/items/add-item" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name">
        <br>
        <label for="category">Category</label>
        <input type="text" name="category" id="category">
        <br>
        <label for="price">Price</label>
        <input type="text" name="price" id="price">
        <br>
        <input type="submit">
    </form>
</main>

</body>
</html>
