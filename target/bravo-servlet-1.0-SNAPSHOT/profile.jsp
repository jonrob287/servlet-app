<%--
  Created by IntelliJ IDEA.
  User: student05
  Date: 7/13/20
  Time: 8:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile Page</title>
    <%@include file="WEB-INF/partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="WEB-INF/partials/navbar.jsp"%>
<div class="jumbotron text-center">
    <h1>Great Job</h1>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Counter Exercise</h5>
                <p class="card-text">Here is the counter Exercise</p>
                <a href="/count" class="btn btn-primary">Go to Count Exercise</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Ping-Pong Exercise</h5>
                <p class="card-text">Here is my Ping-Pong Exercise</p>
                <a href="/ping" class="btn btn-primary">Go to Ping-Pong Exercise</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Wishlist Exercise</h5>
                <p class="card-text">Here is my Wishlist Exercise</p>
                <a href="/items" class="btn btn-primary">Go to Wishlist Exercise</a>
            </div>
        </div>
    </div>
</div>

<%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
