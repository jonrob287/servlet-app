<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student05
  Date: 7/13/20
  Time: 1:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Students</title>
</head>
<body>
<main>
    <h3>Here are all of the students: </h3>
    <table border="1">
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
        </tr>
        <%--    JSTL --%>
        <c:forEach items="${listOfStudents}" var="enrolledStudents">
            <tr>
                <td>${enrolledStudents.firstName}</td>
                <td>${enrolledStudents.lastName}</td>
                <td>${enrolledStudents.email}</td>
            </tr>
        </c:forEach>
    </table>
    <a href="/students/add-student"><button>Add a Student</button></a>
</main>
</body>
</html>
