import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet (name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
//        if (name.equals("admin") && pass.equals("password")){
//            response.sendRedirect("profile.jsp");
//        }else if (name.equals("stevejobs") && pass.equals("apple")){
//            response.sendRedirect("https://www.apple.com");
//        }else if (name.equals("billgates") && pass.equals("microsoft")){
//            response.sendRedirect("https://www.microsoft.com");
//        }
//        else {
//            response.sendRedirect("login.jsp");
//        }
        boolean validAttempt = username.equals("admin") && password.equals("password");
        if (validAttempt) {
            request.getSession().setAttribute("user", true);
            //figured out if the login attempt is good

            //send the "user" to our admin profile
            request.getSession().setAttribute("username", username);
            response.sendRedirect("/my-admin-profile");
        } else {
            //if login attempt is not good
            response.sendRedirect("/login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        boolean isUser = false;

        if (session.getAttribute("user") != null) {
            isUser = (boolean) session.getAttribute("user");

        }

        if (isUser) {
            request.getRequestDispatcher("/my-admin-profile").forward(request, response);
        } else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }
}


        //        String name = response.getParameter("name");
//        PrintWriter writer = response.getWriter();
//        if (name == null) {
//            writer.println("<h1>Hello World!</h1>");
//        }else if(name.equals("stevejobs")){
//            response.sendRedirect("https://www.apple.com");
//        }
//
//        else{
//            writer.println("<h1>Hello " + name + "!</h1>");
//        }
//    }
//}

