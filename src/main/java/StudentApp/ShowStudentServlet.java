package StudentApp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowStudentServlet", urlPatterns = "/students")
public class ShowStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Incorporate our DAO

        // Use the factory to get the dao object
        Students studentsDao = DaoFactory.getStudentsDao();

        //Use a method on the dao to get all of the students
        List<Student> students = studentsDao.all();

        // pass the "data" to the jsp
        request.setAttribute("listOfStudents", students);
        request.getRequestDispatcher("/student-app/students.jsp").forward(request, response);
    }
}
