package StudentApp;

import java.util.ArrayList;
import java.util.List;

// DAO Implementation
public class ListStudents implements Students{

    //Incorporating our DAO
    private List<Student> students = new ArrayList<>();

    //create an empty list of students
    public ListStudents(){
        insert(new Student("Leslie", "Knope", "leslie@pawnee.com"));
        insert(new Student("Ron", "Swanson", "ron@pawnee.com"));
        insert(new Student("Ben", "Wyatt", "ben@pawnee.com"));
    }

    @Override
    public void insert(Student student) {
        this.students.add(student);
    }
    @Override
    public List<Student> all() {
        return this.students;
    }
}
