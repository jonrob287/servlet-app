package CodeBoundWishListApp;

public class DaoFactory {
    private static Items itemsDao;
    public static Items getItemsDao(){
        if(itemsDao == null){
            itemsDao = new ListItems();
        }
        return itemsDao;
    }
}
