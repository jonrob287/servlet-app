package CodeBoundWishListApp;

import StudentApp.Student;

import java.util.List;

public interface Items {
    List<Item> all();

    void insert(Item item);
}
