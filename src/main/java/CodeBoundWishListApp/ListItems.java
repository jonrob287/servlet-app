package CodeBoundWishListApp;

import java.util.ArrayList;
import java.util.List;

public class ListItems implements Items{

    private List<Item> items = new ArrayList<>();
    public ListItems(){
        insert(new Item("Pool Table", "Outdoor", 1299.99));
        insert(new Item("Pool", "Outdoor", 599.99));
        insert(new Item("Samsung Tv", "Electronics", 999.99));
    }

    @Override
    public void insert(Item item) {
        this.items.add(item);
    }

    @Override
    public List<Item> all() {
        return this.items;
    }
}
