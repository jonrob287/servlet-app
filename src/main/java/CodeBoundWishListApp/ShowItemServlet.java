package CodeBoundWishListApp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowItemServlet", urlPatterns = "/items")
public class ShowItemServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Items itemsDao = DaoFactory.getItemsDao();


        List<Item> items = itemsDao.all();


        request.setAttribute("listOfItems", items);
        request.getRequestDispatcher("/wishList-app/items.jsp").forward(request, response);

    }
}
