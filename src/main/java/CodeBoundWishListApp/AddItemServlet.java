package CodeBoundWishListApp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddItemServlet", urlPatterns = "/items/add-item")
public class AddItemServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Items itemsDao = DaoFactory.getItemsDao();

        String newName = request.getParameter("name");
        String newCategory = request.getParameter("category");
        double newPrice =  Double.parseDouble(request.getParameter("price"));

        Item newItem = new Item(newName, newCategory, newPrice);

        itemsDao.insert(newItem);
        response.sendRedirect("/items");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/wishList-app/add-items.jsp").forward(request, response);
    }
}
