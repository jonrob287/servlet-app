import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloServlet", urlPatterns = "/hello")

public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        //get the PrintWriter
        PrintWriter writer = response.getWriter();
        //generate the HTML content
        writer.println("<h1> Hello from Hello Servlet </h1>");
        writer.println("<h3> Hello from update Servlet </h3>");
        writer.println("<a href=\"profile.jsp\"><button>Back</button></a>");

    }
}
