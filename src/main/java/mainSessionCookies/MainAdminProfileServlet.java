package mainSessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "MainAdminProfileServlet", urlPatterns = "/main-admin-profile")
public class MainAdminProfileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        boolean isUser = false;

        if (session.getAttribute("user") !=null) {
            isUser = (boolean) session.getAttribute("user");

        }

        if (isUser){
            request.setAttribute("username", session.getAttribute("username"));
            request.getRequestDispatcher("/mainCookiesSessions/main-admin-profile.jsp").forward(request, response);
        }else {
            request.getRequestDispatcher("/mainCookiesSessions/main-admin-login.jsp").forward(request, response);
        }
    }
}
