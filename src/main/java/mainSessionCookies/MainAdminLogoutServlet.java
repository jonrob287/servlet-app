package mainSessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "MainAdminLogoutServlet", urlPatterns = "/main-admin-logout")
public class MainAdminLogoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //destroys the entire session object, hence logs out the user
        request.getSession().invalidate();

        //redirects the user to admin login page
        response.sendRedirect("/main-admin-login");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //destroys the entire session object, hence logs out the user
        request.getSession().invalidate();

        //redirects the user to admin login page
        response.sendRedirect("/main-admin-login");
    }
}
