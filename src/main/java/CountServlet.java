import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CountServlet", urlPatterns = "/count")
public class CountServlet extends HttpServlet {
    private int counter = 0;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

//    public void counter() {
//        counter = 0;
//    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        counter++;
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("<h2>The count is: " + counter + "</h2>");
        writer.println("<p><a href=\"/hello\">Go to Hello Servlet</a><p>");
        writer.println("\n<a href=\"profile.jsp\"><button>Back</button></a>");

    }

}
